#!/bin/bash
#
# VENUS Combo: s.engine + OPCUA CAEN Server (.ua, licensed) + glue code + VENUS Pilot Server (ua.licensed)
#
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64:/venus/lib:/3rdPartySoftware/boost_1_75_0/stage/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/3rdPartySoftware/UnifiedAutomation/1.5.5/sdk/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/usr/lib:/usr/lib64:/venus/lib

# fix lib names to generic for cc7.8
ln -s /usr/lib64/libprotoc.so.8 /usr/lib64/libprotoc.so
ln -s /usr/lib64/libprotobuf-c.so.1 /usr/lib64/libprotobuf-c.so
ln -s /usr/lib64/libczmq.so.3 /usr/lib64/libczmq.so

SENGINE=caenSimEngine.bin
PILOT=OpcUaVenusPilotServer.open6
# .ua
OPCCAEN=OpcUaCaenServer
SIMCONFIG="$SIM_CONFIG"

# the simulation config can be 
# 1-empty: we take the defualt
# 2-provided as env argument, i.e. like: docker run  -e SIM_CONFIG=sim_BASIC.short.xml --net=host --expose=4901 --expose=23400 <imageID>
# 3-injected during runtime into the contaner where it is picked up i.e. like: docker cp ./mysim.xml <containerID>:/sim_config.xml
echo "picked up from environment: SIMCONFIG= ""${SIMCONFIG}"
if [ -n "${SIMCONFIG}" ]; then
   echo "starting with "${SIMCONFIG}" as electronic tree"
else
   SIMCONFIG=sim_DEFAULT.xml
fi
cp /venus/sim/${SIMCONFIG} /sim_config.xml
#
#
# s. engine
echo "===starting simulation engine==="
#cd /venus/sim && ./$SENGINE -shorthand -cfg /$SIMCONFIG &
cd /venus/sim && ./$SENGINE -shorthand -cfg /sim_config.xml &
echo "Now sleeping five seconds to give the s.engine time enough to start up fully"
sleep 5
#
# run the Unified Automation sdk-based flavour of the OpcUa CAEN server
# discovery mode, using config.xml
echo "===starting OPC .ua server==="
cd /venus/opc && ./$OPCCAEN --hardwareDiscoveryMode &
echo "Now sleeping five seconds to give the Caen OPC server time enough to start up fully"
sleep 5

# run the OPC Venus pilot server, ua
echo "===starting OPC Venus Pilot server==="
cd /venus/pilot && ./$PILOT &

# the above stuff is all run in the background, but this script here must never exit
# so, here we put the supervisor loop which looks for new injected simulation configs 
# and restarts the combo - all three tasks
# this superisor loop never exits
rm -v /sim_config.xml
echo "inject a simulation config into /sim_config.xml (shorthand)"
while true
	do
	if [ -f /sim_config.xml ]
	then
	    echo "found a new /sim_config.xml, stopping combo and restart with the new config"
        sleep 1
	    
	    kill -KILL ` ps ax | grep $OPCCAEN | awk '{printf("%s\n", $1)}' `
	    kill -KILL ` ps ax | grep $PILOT   | awk '{printf("%s\n", $1)}' `
	    kill -KILL ` ps ax | grep $SENGINE | awk '{printf("%s\n", $1)}' `

        echo "===re-starting simulation engine==="
        cd /venus/sim && ./$SENGINE -shorthand -cfg /sim_config.xml &
	echo "Now sleeping ten seconds to give the s.engine time enough to start up fully"
        sleep 10

        echo "===starting CAEN OPC .ua server==="
        cd /venus/opc && ./$OPCCAEN --hardwareDiscoveryMode &
        echo "Now sleeping five seconds to give the Caen OPC server time enough to start up fully"
        sleep 5

        echo "===starting OPC Venus Pilot server==="
        cd /venus/pilot && ./$PILOT &
        sleep 5
        
	echo $SIMCONFIG" done, removing file. Inject a new one if you like sth else."
        rm /sim_config.xml
        echo "==="
        echo "inject a simulation config like docker cp newConfig.xml mycontainer:/sim_config.xml"
        echo "==="
	fi
    sleep 1
done

#echo "keeping docker alive "
#while true
#do
#    sleep 10
#done
