#!/bin/bash
# build all docker images in the correct  sequence, respecting the dependencies.
dreg="gitlab-registry.cern.ch/mludwig/docker"

for df in `cat $1`; do
    tag=""
    rm -f ./.tmp
    echo "(re-)building / checking image "${df}
    docker build -f ${df} . | grep "Successfully built" > ./.tmp
    tag=`cat ./.tmp | sed "s/Successfully built //g"`
    echo "tag= "${tag}
    rm -f ./.tmp


    # tags are the suffix of the Dockerfiles.<suffix>
    suffix=`echo ${df} | sed "s/Dockerfile.//g" | sed "s/\.\///g"`
    echo "image="${dreg}:${suffix}

    # push it if the tag exists
    if [ -z "$tag" ]
    then
        echo "!!! error building image from "${df}" !!!"
        exit
    else
        docker tag ${tag} ${dreg}:${suffix}
        docker push ${dreg}:${suffix}
        echo "===OK OK OK happy happy ======================="
        echo "=== pushed image from "${df}" to "${dreg}:${suffix}" ===="
        echo "===OK OK OK happy happy ======================="
    fi
    sleep 5

done
 
