#!/bin/bash
# build all docker images in the correct  sequence, respecting the dependencies.
# $1 = input a Dockefile sequence list
# dreg="gitlab-registry.cern.ch/mludwig/docker"
#
# correct order in image_sequence.txt, no comments allowed there
for df in `cat $1`; do
    echo " " 
    echo "=========================="
    echo "building image from  ${df}"
    echo "--------------------------"
    podman build -f ${df} .

    # tags are the suffix of the Dockerfiles.<suffix>
    suffix=`echo ${df} | sed "s/Dockerfile.//g" | sed "s/\.\///g"`
    echo "image="${dreg}:${suffix}


done
 
