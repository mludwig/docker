# escape=`

# Use the latest Windows Server Core 2022 image.
#FROM mcr.microsoft.com/windows/servercore:ltsc2022
FROM registry.cern.ch/mcr.microsoft.com/windows/server:ltsc2022


# Restore the default Windows shell for correct batch processing.
SHELL ["cmd", "/S", "/C"]

ADD ./w22scripts/Install-Chocolatey.ps1 /
RUN powershell -file Install-Chocolatey.ps1

# boost
RUN mkdir -p C:\3rdPartySoftware\boost `
	&& cd C:/3rdPartySoftware/boost `
	&& curl -L -o ./boost_1_84_0.zip https://boostorg.jfrog.io/artifactory/main/release/1.84.0/source/boost_1_84_0.zip `
	&& unzip boost_1_84_0.zip 


RUN choco install -y visualstudio2019buildtools --norestart --package-parameters "--add Microsoft.VisualStudio.Workload.NativeDesktop --includeRecommended"
#
# Software installed to 'C:\ProgramData\chocolatey\lib\vswhere'
RUN choco install -y vswhere
# set the path
RUN $env:Path+='C:\ProgramData\chocolatey\lib\vswhere\tools'
# [Environment]::SetEnvironmentVariable ("Path", $env:Path, [System.EnvironmentVariableTarget]::Machine)

# not sure how to integrate this with gitlab and test
#docker exec -ti <containerID> "C:\\Program Files (x86)\\Microsoft Visual Studio\\2022\\BuildTools\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe"
# use like : docker run d278689bc64b "pwd & dir"
# docker run d278689bc64b "pwd & dir & cd c:\\3rdPartySoftware\\boost\\boost_1_84_0 & dir"
# docker run d278689bc64b "pwd & dir & cd c:\\3rdPartySoftware\\boost\\boost_1_84_0 & bootstrap.bat"
#

#  C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\Common7\Tools> .\Launch-VsDevShell.ps1

#ENTRYPOINT ["C:\\Program Files (x86)\\Microsoft Visual Studio\\2022\\BuildTools\\Common7\\Tools\\VsDevCmd.bat", "&&", "powershell.exe", "-NoLogo", "-ExecutionPolicy", "Bypass"]

COPY ./entrypoint.cmd /
ENTRYPOINT entrypoint.cmd


#RUN cd C:/3rdPartySoftware/boost/boost_1_84_0 `
#	&& bootstrap.bat `
#	&& b2 -j6 link=static,shared 

#


