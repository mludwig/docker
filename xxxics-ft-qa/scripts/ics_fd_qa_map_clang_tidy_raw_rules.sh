#!/bin/bash
# map raw clang-tidy rules to sonarq rules using a list
# $1=raw rule
#
#echo "raw rule= "$1
RULES_MAP=./scripts/clang_tidy_rules_map.txt
cat ${RULES_MAP} | grep -v "#" | grep $1 | awk '{print $2}'

