#!/bin/bash
#
# clang-tidy static code analysis 
#
echo "===executing script $0 START==="
echo "project subdir which contains C++ source= $1"
echo "sonar project name (no whitespaces)= $2"
echo "sonar project version string (no whitespaces)= $3"
#
# we output a sonar-project.properties file which fits the project
if [ -z $1 ] 
then
	echo "please provide a directory with a C++ code tree"
	exit
fi
if [ -z $2 ] 
then
	echo "please provide a sonar project name (no whitespaces)"
	exit
fi
if [ -z $3 ] 
then
	echo "please provide a sonar project version string (no whitespaces)"
	exit
fi
# WIN32 i.e.


# reports are relative to the SRCTREE
REPORT_DIR0=$CACHE_DIR/clang-tidy-report
ISSUE_DIR=$CACHE_DIR/issues
mkdir -p ${REPORT_DIR0}
mkdir -p ${ISSUE_DIR}
echo "REPORT_DIR0="${REPORT_DIR0}
echo "ISSUE_DIR="${ISSUE_DIR}

#
# source scope: all files are explicitly listed in a filelist
echo "===looking for cpp/h source files, making a list from: "$1"==="
FLIST=$REPORT_DIR0/filelist
rm -rf ${FLIST}; touch ${FLIST}
PATTERN="c cpp cc C CC CPP h hpp H HPP C++ C++ h++ H++ cxx CXX hxx HXX"
for p in $PATTERN; do
	find $1 -name "*.${p}" | grep -v "metadata" >> ${FLIST}
done
echo "wrote list of src files into  ${FLIST}"
echo "===source scope is in filelist "${FLIST}" ==="
cat ${FLIST}
echo "================================="

# check compile_commands.json
find ./ -name "compile_commands.json" -exec cat {} \;

# clang-tidy, using .clang-tidy configuration, /usr/local/bin as installed
clang-tidy --version
echo "===clang tidy configuration start==="
clang-tidy -dump-config
echo "===clang tidy configuration end==="
echo "===clang tidy active rules start==="
clang-tidy -list-checks
echo "===clang tidy active rules end==="

echo "===scanning files from list==="

echo ${FLIST}
ls -l ${FLIST}
echo "=============================="
for fn in `cat ${FLIST}`; do
    fname=`basename ${fn}`
    dname=`dirname ${fn}`
 
    # save file to org before touching it with FIX-IT
    cp ${fn} ${fn}.org
    
    echo "===analysing file "${fn}" , dumping report into ${REPORT_DIR0}/${fname}.xml / ast"

    echo "clang-check ${fn} -ast-dump -- > ${REPORT_DIR0}/${fname}.ast"
    clang-check ${fn} -ast-dump -- > ${REPORT_DIR0}/${fname}.ast

## with warnings-as-errors flag, but need to suppress it from the ctr
## the comma in the rule-key name is not accepted by sonarqube
#    echo "clang-tidy  -store-check-profile=${REPORT_DIR0} ${fn} --warnings-as-errors='*' -extra-arg -U_WIN32 -extra-arg -UWIN32 | sed \"s/--warnings-as-errors//g\" > ${REPORT_DIR0}/${fn}.ctr"
#    clang-tidy -store-check-profile=${REPORT_DIR0}/${fname}.json  ${fn} --warnings-as-errors='*' -extra-arg -U_WIN32 -extra-arg -UWIN32 | sed "s/--warnings-as-errors//g" > ${REPORT_DIR0}/${fname}.ctr

# without flag: works
    echo "clang-tidy  -store-check-profile=${REPORT_DIR0} ${fn} -extra-arg -U_WIN32 -extra-arg -UWIN32 > ${REPORT_DIR0}/${fn}.ctr"
    clang-tidy -store-check-profile=${REPORT_DIR0}/${fname}.json  ${fn} -extra-arg -U_WIN32 -extra-arg -UWIN32 > ${REPORT_DIR0}/${fname}.ctr

    # transformat the output to xml. produces xmls from that crap 
    #./scripts/ics_fd_qa_convertReportsClangTidy2Cppcheck.sh ${REPORT_DIR0}/${fname}.ctr 
done
echo "===done: your reports are in ${REPORT_DIR0}"
echo "===executing script $0 END==="

