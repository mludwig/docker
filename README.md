Readme.md
==========

This projects contains Dockerfiles for hierarchical builds.
Run ./build_all.sh and then tag_and_push_all.sh to recreate all images in
the correct sequence.


IMAGES
======

*.test images are interactive with entry point for debugging, otherwise identical

===based on quasar.cc7===
=========================

Dockerfile.quasar.cc7
---------------------
- gitlab-registry.cern.ch/mludwig/docker:quasar.cc7
- basic quasar with python3, boost, cmake, astyle, .ua skd, and quasar python dependencies. 
- Use as base for specific opcua server builds
- image is not versioned

Dockerfile.opcua-pdu
--------------------
- gitlab-registry.cern.ch/mludwig/docker:opcua-pdu
- based on gitlab-registry.cern.ch/mludwig/docker:quasar.cc7
- image with snmp for building the opcua-pdu server
- scl toolkit 7 needed for mule/gcc 7
- image is not versioned

Dockerfile.opcua-lxi
--------------------
- gitlab-registry.cern.ch/mludwig/docker:opcua-lxi
- based on gitlab-registry.cern.ch/mludwig/docker:quasar.cc7
- image with liblxi for building the opcua-lxi server
- image is not versioned

Dockerfile.opcua-venusPilot
--------------------------- 
- gitlab-registry.cern.ch/mludwig/docker:opcua-venusPilot
- based on gitlab-registry.cern.ch/mludwig/docker:quasar.cc7
- has all the zmq and protobuf dependencies
- build image for uademo and open6
- image is not versioned



===based on .ua sdk licensed===
===============================

Dockerfile.quasar.cc7.licensed
------------------------------
- gitlab-registry.cern.ch/mludwig/docker:quasar.cc7.licensed
- from licensed .ua skd image
- old boost, cmake, .ua sdk 
- vanilla image, no adds
- image is not versioned


Dockerfile.opcua-venusPilot.licensed
------------------------------------
- gitlab-registry.cern.ch/mludwig/docker:opcua-venusPilot.licensed
- based on gitlab-registry.cern.ch/mludwig/docker:quasar.cc7.licensed
- has all the zmq and protobuf dependencies
- image is not versioned


Dockerfile.opcua-lxi.licensed
-----------------------------
- gitlab-registry.cern.ch/mludwig/docker:opcua-lxi.licensed
- based on  gitlab-registry.cern.ch/mludwig/docker:quasar.cc7.licensed
- old boost, cmake, .ua sdk 
- updated cc7, addes lxi lib and needed tools
- image is not versioned

Dockerfile.opcua-pdu.licensed
-----------------------------
- gitlab-registry.cern.ch/mludwig/docker:opcua-lxi.licensed
- based on  gitlab-registry.cern.ch/mludwig/docker:quasar.cc7.licensed
- scl toolkit 7 (gcc 7) for snmp/mule
- netsnmp
- image is not versioned



===based on cc7_base latest===
==============================

Dockerfile.can.cc7
------------------
- gitlab-registry.cern.ch/mludwig/docker:can.cc7
- based on cc7_base.latest
- boost 1.73.0, cmake 3.15.2 
- CAN vedor libs anagate, peak, systec
- libsocketcan, xerces
- image is not versioned

Dockerfile.venus-build.cc7
--------------------------
- gitlab-registry.cern.ch/mludwig/docker:venus-build.cc7
- based on cc7_base.latest
- boost 1.73.0, cmake 3.15.2
- zmq, protobuf etc dependencies to build the (caen-) simulation engine
- no quasar/OPC
- image is not versioned


Dockerfile.venus-caen.combo (runtime SaaS)
------------------------------------------
- gitlab-registry.cern.ch/mludwig/docker:venus-caen.combo
- based on cc7_base.latest
- runtime cc7 image for a s.engine, opc and pilot server for CAEN simulation
- caen server v0.9.6
- venus pilot 1.1.2devel
- s.engine 1.1.3
- inject a new electronic tree: docker cp newtree.xml containerID:/sim_config.xml
- the combo will stop and restart with the new electronic tree. 
- run in the foreground:
   docker run  --net=host --expose=4840-4940 --expose=8800-8900 gitlab-registry.cern.ch/mludwig/docker:venus-caen.combo
- inject a opcua-async script into a running "scripting" container #2:
  docker cp ./combo-performance3.py container2ID:/client.py
- image is versioned


Dockerfile.opcua-asyncio.cc7 (runtime SaaS)
-------------------------------------------
- gitlab-registry.cern.ch/mludwig/docker:opcua-asyncio
- runtime image to execute python3 asyncio scripts on cc7 properly installed
- start a container, i.e. like:
  docker run --net=host --expose=4840-4940 --expose=8800-8900 gitlab-registry.cern.ch/mludwig/docker:opcua-asyncio
- inject a script like:
- docker cp ./myscript.py <containerID>:/client.py
- image is not versioned


===QA images based on cc7 and ubuntu ===
========================================

Dockerfile.ubuntu2004.doc
-----------------------------------
- based on ubuntu20.04 (LTS)
- sphinx/breathe/doxygen combo plus scripts to produce documentation from sphinx-source
- tagged/versioned into gitlab-registry.cern.ch/mludwig/docker/ubuntu2004:doc
- not part of kobe or jcopfw/qa/ics-fd-qa, use extra job in project pipeline (i.e. see opcua-pdu)
- image is not versioned

Dockerfile.ubuntu2004.ics-fd-qa.qa
----------------------------------
- based on ubuntu20.04 (LTS)
- llvm12.0 plus tools : cppcheck and sonar-scanner
- image is versioned 
- tagged/versioned into gitlab-registry.cern.ch/jcopfw/qa/ics-fd-qa:qa1.0.4

Dockerfile.cc7.ics-fd-qa.client
-------------------------------
- based on cc7, just scripts for the project client pipeline
- image is versioned 
- tagged/versioned into gitlab-registry.cern.ch/jcopfw/qa/ics-fd-qa:client0.0.2







