# powershell script to install visual studio community on w22
# needs installer 
# see https://learn.microsoft.com/en-us/visualstudio/install/build-tools-container?view=vs-2022
#
#mkdir C:\BuildTools; cd C:\BuildTools
#
# Restore the default Windows shell for correct batch processing.
#SHELL ["cmd", "/S", "/C"]

# Download the Build Tools bootstrapper.
#curl.exe --output vs_BuildTools.exe https://aka.ms/vs/17/release/vs_BuildTools.exe 
    
# Install Build Tools 
dir 
.\vs_BuildTools.exe --quiet --wait --norestart --nocache 
#--remove Microsoft.VisualStudio.Component.Windows10SDK.10240 --remove Microsoft.VisualStudio.Component.Windows10SDK.10586  --remove #Microsoft.VisualStudio.Component.Windows10SDK.14393 --remove Microsoft.VisualStudio.Component.Windows81SDK 
    
# Cleanup
#del /q vs_BuildTools.exe





