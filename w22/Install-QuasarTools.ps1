# powershell script to install quasar needed tools on w22
#
# use nuget
.\nuget.exe install python -ExcludeVersion -OutputDirectory .
c:\python\tools\python.exe -V

#  pip
c:\python\tools\python.exe get-pip.py
c:\python\tools\Scripts\pip3.exe install colorama jinja2 lxml pygit2

# cmake 3.5.2
.\nuget.exe install cmake -ExcludeVersion -OutputDirectory .

# xsdcxx from codeSynthesis: append it into system exe path
$env:Path += ';.\xsd-4.2.0-x86_64-windows10\bin'
echo $Env:PATH
xsd --version
copy .\xsd-4.2.0-x86_64-windows10\bin\xsd.exe .\xsd-4.2.0-x86_64-windows10\bin\xsdcxx.exe 

# xmllint from https://gitlab.gnome.org/GNOME/libxml2/
$env:Path += ';.\xsd-4.2.0-x86_64-windows10\bin'
echo $Env:PATH
