# ===========================================================================
# Script builds boost (C++ library) from source using dedicated python script
# ===========================================================================

# 
# Set script args from invocation, required input:
# 1. sourcezipuri - boost.zip source URI (e.g. https://boostorg.jfrog.io/artifactory/main/release/1.82.0/source/boost_1_82_0.zip)
# 2. rootdir - working location where source will be unzipped, built and installed (tree structure defined in this/python script)
# 
$sourcezipuri=$args[0]
$rootdir=$args[1]
"sourcezipuri [$sourcezipuri]" | Write-Host
"rootdir [$rootdir]" | Write-Host

#
# Set up rootdir
#
Remove-Item -Path $rootdir -Recurse -Force -Confirm:$false -ErrorAction SilentlyContinue
New-Item -Path $rootdir -Type Directory -Force

#
# Clone python build script
#
git clone --recursive https://gitlab.cern.ch/bfarnham/boost_mapped_namespace_builder.git $rootdir

#
# Download boost.zip from URI
#
Invoke-WebRequest -Uri $sourcezipuri -OutFile $rootdir\boost-src.zip

#
# Load up vcvars - vis studio compiler env needed
#
. .\Load-Visual-Studio-Env-In-Powershell.ps1

#
# Run build script (extracts, builds, etc.)
#
Push-Location $rootdir
python boost_mapped_namespace_builder.py --boost_src_zip .\boost-src.zip --base_dir $rootdir --boost_mapped_namespace_nm boost_1_82_0
Pop-Location

#
# Patch snprintf problem in final boost release
#
$hdr="$rootdir\MAPPED_NAMESPACE_INSTALL\include\boost-1_82\boost\assert\source_location.hpp"
(Get-Content "$hdr").replace('std::snprintf', '_snprintf') | Set-Content "$hdr"

#
# Delete intermediate build cruft (only install-dir needed), stand back and admire the work
#
Remove-Item -Path $rootdir\boost-src.zip -Recurse -Force -Confirm:$false -ErrorAction SilentlyContinue
Remove-Item -Path $rootdir\DEFAULT_NAMESPACE* -Recurse -Force -Confirm:$false -ErrorAction SilentlyContinue
Remove-Item -Path $rootdir\MAPPED_NAMESPACE -Recurse -Force -Confirm:$false -ErrorAction SilentlyContinue
tree /F $rootdir\MAPPED_NAMESPACE_INSTALL