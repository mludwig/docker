# Visual Studio 2022 Build Tools
choco install -y --no-progress visualstudio2022buildtools

# C++ Component for Visual Studio 2019 with all extras
choco install -y --no-progress visualstudio2022-workload-vctools

# Link Professional folder to BuildTools for compatibility with existing
# building scripts
#New-Item -ItemType SymbolicLink -Path "C:\Program Files (x86)\Microsoft Visual Studio\2022\Professional" -Target "C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools"

# Clean-up
#Remove-Item -Path "C:\Program Files (x86)\Microsoft Visual Studio\Installer" -Recurse -Force

