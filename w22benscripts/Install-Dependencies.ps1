$ErrorActionPreference = "Stop" 

# Install chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

# Make `refreshenv` available right away, by defining the $env:ChocolateyInstall
# variable and importing the Chocolatey profile module.
# Note: Using `. $PROFILE` instead *may* work, but isn't guaranteed to.
$env:ChocolateyInstall = Convert-Path "$((Get-Command choco).Path)\..\.."   
Import-Module "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"

# Miscelaneous dependencies
choco install -y --no-progress python311 git nano strawberryperl 7zip.install astyle powershell-core
choco install -y --no-progress cmake --installargs 'ADD_CMAKE_TO_PATH=System'
choco install -y --no-progress ninja

# NB: both steps below required for wixtoolset
Install-WindowsFeature Net-Framework-Core
choco install -y --no-progress wixtoolset

# quasar requirement - some scripts use python3 calls
New-Item -ItemType SymbolicLink `
-Path "C:\Python311\python3.exe" -Target "C:\Python311\python.exe"

# Clean-up choco
Remove-Item -Path "C:\Users\ContainerAdministrator\AppData\Local\Temp\chocolatey" -Recurse -Force

# refreshenv for python, then install python libs for quasar
refreshenv
python.exe -m pip install --upgrade pip lxml jinja2 colorama pygit2