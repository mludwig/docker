#
# Script loads visual studio x64 C++ compiler environment into calling powershell
# usage from calling powershell session is (note preceeding dot below - is called 'dot sourcing')
# . C:\your\path\to\script\Load-Visual-Studio-Env-In-Powershell.ps1
#
echo "loading visual studio environment vars..."
New-Item -Path C:\temp -Type Directory -Force

# msbuild tools vcvars path
cmd.exe /c "call `"C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Auxiliary\Build\vcvars64.bat`" && set > C:\temp\vcvars.txt"

# vis studio community vcvars path (should be commented out - only for debugging on machines with VS community installations)
#cmd.exe /c " call `"C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvars64.bat`" && set > C:\temp\vcvars.txt"

Get-Content "C:\temp\vcvars.txt" | Foreach-Object {
   if ($_ -match "^(.*?)=(.*)$") {
     Set-Content "env:\$($matches[1])" $matches[2]
   }
}