# ====================================================================
# Script builds libxml2 dependency required for Unified Automation SDK
# ====================================================================

# 
# Set script args from invocation, required input:
# 1. sourcezipuri - libxml2.zip source URI (e.g. https://github.com/GNOME/libxml2/archive/refs/tags/v2.11.3.zip)
# 2. rootdir - location where source will be unzipped, built and installed (tree structure defined in this script)
# 
$sourcezipuri=$args[0]
$rootdir=$args[1]
$srcdir="$rootdir\src"
$installdir="$rootdir\install-release"
"sourcezipuri [$sourcezipuri]" | Write-Host
"rootdir [$rootdir]" | Write-Host
"srcdir [$srcdir]" | Write-Host
"installdir [$installdir]" | Write-Host

#
# Set up tree structure under rootdir
#
Remove-Item -Path $rootdir -Recurse -Force -Confirm:$false -ErrorAction SilentlyContinue
New-Item -Path $rootdir -Type Directory -Force
New-Item -Path $installdir -Type Directory

#
# Download libxml2.zip from URI
#
Invoke-WebRequest -Uri $sourcezipuri -OutFile $rootdir\libxml2-src.zip

#
# Extract libxml2 source, delete zip (then rename output dir to rootdir/src)
#
7z x $rootdir\libxml2-src.zip -o"$rootdir"
Remove-Item -Path $rootdir\libxml2-src.zip -Force
Get-ChildItem $rootdir -Directory -Filter "libxml*" | Rename-Item -NewName $srcdir

#
# Load up vcvars - vis studio compiler env needed
#
. .\Load-Visual-Studio-Env-In-Powershell.ps1

#
# Configure release (need to be in $srcdir/win32) and build
#
Push-Location $srcdir\win32
cscript configure.js compiler=msvc prefix=$installdir ftp=no http=no iconv=no zlib=no
nmake /f Makefile.msvc install
Pop-Location

#
# Delete source (only install-dir needed), stand back and admire the work
#
Remove-Item -Path $srcdir -Recurse -Force -Confirm:$false -ErrorAction SilentlyContinue
tree /F $installdir