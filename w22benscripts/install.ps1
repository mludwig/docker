$ErrorActionPreference = "Stop" 

.\Install-Dependencies.ps1
.\Install-Visual-Studio.ps1
.\Install-xsd.ps1 https://www.codesynthesis.com/download/xsd/4.0/windows/i686/xsd-4.0.msi
.\Build-Libxml2.ps1 https://github.com/GNOME/libxml2/archive/refs/tags/v2.11.3.zip C:\3rdParty\libxml2
#.\Install-Boost-MSVC-14.1.ps1
#.\Build-Openssl.ps1 https://github.com/openssl/openssl/archive/refs/tags/openssl-3.1.0.zip C:\3rdParty\openssl
#.\Build-Unified-Automation-SDK.ps1 C:\UaSdkCppBundleSource.zip C:\3rdParty\UnifiedAutomation
#.\Build-Boost.ps1 https://boostorg.jfrog.io/artifactory/main/release/1.82.0/source/boost_1_82_0.zip C:\3rdParty\boost
#.\Build-Net-Snmp.ps1 https://github.com/net-snmp/net-snmp/archive/refs/tags/v5.9.3.zip C:\3rdParty\net-snmp
#.\Build-Xerces.ps1 https://archive.apache.org/dist/xerces/c/3/sources/xerces-c-3.1.4.zip C:\3rdParty\xerces
#.\Build-Qt5Base.ps1 https://code.qt.io/qt/qtbase.git v5.15.10-lts-lgpl C:\3rdParty\qtbase C:\3rdParty\openssl\install-release