# ==================================================================
# Script installs CodeSynthesis xsd for windows (quasar requirement)
# Note default installation requires some fixing (see replace calls)
# ==================================================================

# 
# Set script args from invocation, required input:
# 1 msiuri - msi installer URI (e.g. https://www.codesynthesis.com/download/xsd/4.0/windows/i686/xsd-4.0.msi)
# 
$msiuri=$args[0]
"msiuri [$msiuri]" | Write-Host

#
# download msi and install (and remove installer)
# Note installdir - will change if later xsd installed
#
Invoke-WebRequest -UseBasicParsing -Uri $msiuri -OutFile .\xsd.msi
Start-Process msiexec.exe -Wait -ArgumentList '/I C:\xsd.msi /quiet'
Remove-Item -Path .\xsd.msi -Force -Confirm:$false -ErrorAction SilentlyContinue
$installdir="C:\Program Files (x86)\CodeSynthesis XSD 4.0"
ls $installdir

#
# Patch template header (require explicit xerces namespace)
# NB: later versions of xsd might need to fix this path
#
$templatehdr="$installdir\include\xsd\cxx\tree\serialization.txx"
(Get-Content "$templatehdr").replace('DOMDocument& doc', 'xercesc::DOMDocument& doc') `
 | Set-Content "$templatehdr"
(Get-Content "$templatehdr").replace('const DOMElement& se', 'const xercesc::DOMElement& se') `
 | Set-Content "$templatehdr"
(Get-Content "$templatehdr").replace('DOMNamedNodeMap& sa', 'xercesc::DOMNamedNodeMap& sa') `
 | Set-Content "$templatehdr"

#
# quasar scripts expect xsdcxx; make symlink
#
New-Item -ItemType SymbolicLink `
 -Path "$installdir\bin\xsdcxx.exe" -Target "$installdir\bin\xsd.exe"

#
# extend system PATH var
#
[Environment]::SetEnvironmentVariable("Path", "$Path;$installdir\bin")
refreshenv