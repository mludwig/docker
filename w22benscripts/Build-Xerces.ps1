# ========================================================================
# Script builds xercesc lib for windows (required by multiple OPC servers
# ========================================================================

#
# Set script args from invocation, required input:
# 1. sourcezipuri - xerces source URI (e.g. https://archive.apache.org/dist/xerces/c/3/sources/xerces-c-3.1.4.zip)
# 2. rootdir - location where source will be unzipped, built and installed (tree structure defined in this script)
#
$sourcezipuri=$args[0]
$rootdir=$args[1]
$srcdir="$rootdir\src"
$installdir="$rootdir\install-release"
"sourcezipuri [$sourcezipuri]" | Write-Host
"rootdir [$rootdir]" | Write-Host
"srcdir [$srcdir]" | Write-Host
"installdir [$installdir]" | Write-Host

#
# Set up tree structure under rootdir
#
Remove-Item -Path $rootdir -Recurse -Force -Confirm:$false -ErrorAction SilentlyContinue
New-Item -Path $rootdir -Type Directory -Force
New-Item -Path $installdir -Type Directory

#
# Download xerces source from URI
#
Invoke-WebRequest -Uri $sourcezipuri -OutFile $rootdir\xerces-src.zip

#
# Extract source, delete zip (then rename output dir to rootdir/src)
#
7z x $rootdir\xerces-src.zip -o"$rootdir"
Remove-Item -Path $rootdir\xerces-src.zip -Force
mv $rootdir\xerces-c-3.1.4\ $srcdir

#
# Load up vcvars - vis studio compiler env needed
#
. .\Load-Visual-Studio-Env-In-Powershell.ps1

#
# build via msbuild
#
Push-Location $srcdir\projects\Win32\VC14\xerces-all\XercesLib
Msbuild XercesLib.vcxproj -t:Rebuild -p:PlatformToolset=v143 -p:Configuration=Release -p:ConfigurationType=StaticLibrary
Pop-Location

#
# Install seems a bit flaky; copy what we need to install dir
#
Copy-Item -Path $srcdir\src\xercesc -Filter "*.hpp" -Destination $installdir\include -Recurse -Force -Confirm:$false
Copy-Item -Path $srcdir\src\xercesc -Filter "*.h" -Destination $installdir\include -Recurse -Force -Confirm:$false
Copy-Item -Path $srcdir\Build\Win64\VC14\Release -Filter xerces-c*.* -Destination $installdir\lib -Recurse -Force -Confirm:$false


#
# Remove cruft
#
Remove-Item -Path "$srcdir" -Recurse -Force -Confirm:$false

tree /f $installdir