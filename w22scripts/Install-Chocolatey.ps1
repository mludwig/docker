# powershell script to install chocolatey package manager  on w22
#
powershell Set-ExecutionPolicy unrestricted
powershell Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = 	[System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object 	System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
choco --version
choco feature enable -n=allowGlobalConfirmation
#
choco install unzip --quiet
choco install curl --quiet
#
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
refreshenv
# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')


