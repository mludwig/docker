REM cmd script to install visual studio community 2022 on w22
REM
REM choco

REM choco install -y visualstudio2022buildtools  --quiet --norestart --add nativeDesktop --includeRecommended --package-parameters "--passive --locale en-US"
 
REM save modified machine path
REM [System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')

REM choco install -y visualstudio2019buildtools --norestart --add nativeDesktop --includeRecommended

REM choco install -y visualstudio2019buildtools --norestart --package-parameters "--add Microsoft.VisualStudio.Workload.NativeDesktop --includeRecommended"
choco install -y visualstudio2019buildtools --norestart --package-parameters "--add Microsoft.VisualStudio.Workload.NativeDesktop --includeRecommended"
