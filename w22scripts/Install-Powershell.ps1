# powershell script to install powershell into the image on w22
# without having to have to 100MB installer on disk: we use choco
#
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
#
# needs installer 
# see https://learn.microsoft.com/en-us/visualstudio/install/build-tools-container?view=vs-2022
# but can be done via choco as well
#mkdir C:\BuildTools; cd C:\BuildTools
#
# install powershell into the image since it is not provided
# see https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.4#installing-the-msi-package
# curl.exe --output PowerShell-7.4.1-win-x64.msi https://github.com/PowerShell/PowerShell/releases/download/v7.4.1/PowerShell-7.4.1-win-x64.msi
# msiexec.exe /package PowerShell-7.4.1-win-x64.msi /quiet ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1 ADD_FILE_CONTEXT_MENU_RUNPOWERSHELL=1 ENABLE_PSREMOTING=1 #REGISTER_MANIFEST=1 USE_MU=1 ENABLE_MU=1 ADD_PATH=1

choco install powershell-core --install-arguments='"DISABLE_TELEMETRY=1"' --quiet
#
# other tools
choco install unzip --quiet

refreshenv
# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')
