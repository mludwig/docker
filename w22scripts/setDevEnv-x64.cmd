
@echo off
REM call VS command prompt to set up the standard environment VS2022. We rely on choco community build tools
REM VS professional:
REM call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64
REM
REM VS community edition:
call "C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\Common7\Tools\VsDevCmd.bat" -arch=x64
REM call %API_ROOT%\checkApiEnv.cmd x64
