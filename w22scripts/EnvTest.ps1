# EnvTest.ps1
echo "===running EnvTest.ps1==="

echo "reading from machine all env= "
[System.Environment]::GetEnvironmentVariables([System.EnvironmentVariableTarget]::Machine)

echo "reading from machine path Path= "
[System.Environment]::GetEnvironmentVariable('Path') 

echo "===finished EnvTest.ps1==="
