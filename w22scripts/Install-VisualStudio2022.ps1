# powershell script to install visual studio 2022 on w22
#
# copied from 
# https://gitlab.cern.ch/quasar-team/docker/-/blob/BE-ICS-add-ws22-docker-quasar-with-unified-automation-build-inputs/ws22_UnifiedAutomation/Install-Visual-Studio.ps1?ref_type=heads
# for now
# licensed version

## Visual Studio 2022 Build Tools
#choco install -y --no-progress visualstudio2022buildtools

## C++ Component for Visual Studio 2019 with all extras
#choco install -y --no-progress visualstudio2022-workload-vctools

# use community version
choco install -y visualstudio2022community  --quiet --norestart --package-parameters "--add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Workload.MSBuildTools --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --includeRecommended --passive --locale en-US" 


# grab the environment from VS and inject it into powershell
if (-not (Test-Path env:API_ROOT)) {
        Write-Host "Calling setDevEnv-x64.cmd script to set up environment"
    CMD /c "C:\setDevEnv-x64.cmd && set" | .{process {if ($_ -match '^([^=]+)=(.*)') { [Environment]::SetEnvironmentVariable($matches[1],$matches[2],'Process');}}}
} else {
        Write-Host "Development Environment already set"
}




# Link Professional folder to BuildTools for compatibility with existing
# building scripts
New-Item -ItemType SymbolicLink -Path "C:\Program Files (x86)\Microsoft Visual Studio\2022\Professional" -Target "C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools"

# Clean-up
Remove-Item -Path "C:\Program Files (x86)\Microsoft Visual Studio\Installer" -Recurse -Force




#
# choco
#Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
#choco install -y visualstudio2022buildtools  --quiet --norestart --package-parameters "--allWorkloads --includeRecommended --includeOptional --passive --#locale en-US"
# 

#
#refreshenv
# save modified machine path
#[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')




