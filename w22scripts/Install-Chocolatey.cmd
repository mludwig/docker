REM cmd script to install chocolatey package manager  on w22
REM

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

choco --version
choco feature enable -n=allowGlobalConfirmation
#
choco install unzip --quiet
choco install curl --quiet
#
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
refreshenv
# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Process')


