# powershell script to install boost1.84.0 build from scratch  into the image on w22
#
#
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
#
mkdir C:\3rdPartySoftware\boost; cd C:\3rdPartySoftware\boost
curl.exe -L -o .\boost_1_84_0.zip https://boostorg.jfrog.io/artifactory/main/release/1.84.0/source/boost_1_84_0.zip
unzip .\boost_1_84_0.zip
#
# that is injected from the cmake toolchain but lets set it here for info
$env:Boost_Root = 'C:\3rdPartySoftware\boost\boost_1_84_0'
$env:Path += ';$env:Boost_Root'
refreshenv
# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')
[System.Environment]::SetEnvironmentVariable('Boost_Root',$env:Boost_Root, 'Machine')
