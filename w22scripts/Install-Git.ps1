# powershell script to install git on w22
#
#.\Git-2.43.0-64-bit.exe /VERYSILENT /NORESTART
#
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
#
choco install -y git --params "/GitAndUnixToolsOnPath /WindowsTerminal /NoAutoCrlf"
refreshenv
git --version
# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')
