# powershell script to install boost and cmake on w22 
#
choco install -y boost-msvc-14.1
choco install -y cmake.portable --installargs 'ADD_CMAKE_TO_PATH=System' --apply-install-arguments-to-dependencies
