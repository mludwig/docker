# powershell script to install quasar needed tools on w22
#
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
#
choco install -y cmake --installargs "ADD_CMAKE_TO_PATH=System"
refreshenv
cmake --version
#
choco install -y python 
refreshenv
python --version
##  pip
python get-pip.py
pip3 install colorama jinja2 lxml pygit2
#
# xsdcxx from codeSynthesis: append it into system exe path
$env:Path += ';.\xsd-4.2.0-x86_64-windows10\bin;c:\xsd-4.2.0-x86_64-windows10\bin'
echo $Env:PATH
xsd --version
# and a symlink for the poor people
copy .\xsd-4.2.0-x86_64-windows10\bin\xsd.exe .\xsd-4.2.0-x86_64-windows10\bin\xsdcxx.exe 
xsdcxx --version
#
# xmllint from https://gitlab.gnome.org/GNOME/libxml2/
#$env:Path += ';.\xsd-4.2.0-x86_64-windows10\bin;c:\xsd-4.2.0-x86_64-windows10\bin'
#echo $Env:PATH
#refreshenv
# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')



