# powershell script to build boost1.75.0 build from scratch  into the image on w22

# Visual Studio Community
# C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\devenv.exe
#
#  docker exec -ti <containerID> 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat'
# or for build tools install only:
# C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\Common7\Tools\VdDevCmd.bat
#cd C:\3rdPartySoftware\boost\boost_1_75_0\tools\build\v2\engine\src
#.\build.bat

# drop into VS developer shell from here. 
#'C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\Common7\\Tools\\VsDevCmd.bat &&
#	cd C:\3rdPartySoftware\boost\boost_1_75_0 &&
#	.\bootstrap.bat &&
#	.\b2 -j 2 --link=static,shared'
#
#
# we set up the powershell env for VS tools
C:\SetDevEnv-x64.ps1
#

#cd C:\3rdPartySoftware\boost\boost_1_75_0
#.\bootstrap.bat
#.\b2 -j 2 --link=static,shared

#
#
# that is injected from the cmake toolchain but lets set it here for info
$env:Boost_Root = 'C:\3rdPartySoftware\boost\boost_1_75_0'
$env:Path += ';$env:Boost_Root'
#
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
refreshenv
# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')
[System.Environment]::SetEnvironmentVariable('Boost_Root',$env:Boost_Root, 'Machine')
