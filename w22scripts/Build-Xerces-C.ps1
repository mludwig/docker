﻿# dl and build xerces-c
mkdir C:\3rdPartySoftware\
cd  C:\3rdPartySoftware\
Invoke-WebRequest https://dlcdn.apache.org//xerces/c/3/sources/xerces-c-3.2.5.zip -OutFile C:\3rdPartySoftware\xerces-c.zip
unzip C:\3rdPartySoftware\xerces-c.zip
cd C:\3rdPartySoftware\xerces-c-3.2.5
#
mkdir build
cd build
cmake -G "Visual Studio 17 2022" -DCMAKE_INSTALL_PREFIX=D:\libs C:\3rdPartySoftware\xerces-c-3.2.5
cmake --build . --config Debug
ctest -V -C Debug -j 4
cmake --build . --config Debug --target install  
