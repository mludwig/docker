# ps1 script to clone / inject VS environment into this env (powershell)
# calls cmd


Get-Childitem -path env:

# grab the environment from VS and inject it into powershell
if (-not (Test-Path env:API_ROOT)) {
    Write-Host "Calling setDevEnv-x64.cmd script to set up environment"
    CMD /c ".\setDevEnv-x64.cmd && set" | .{process {if ($_ -match '^([^=]+)=(.*)') { [Environment]::SetEnvironmentVariable($matches[1],$matches[2],'Machine');}}}
} else {
    Write-Host "Development Environment already set"
}
