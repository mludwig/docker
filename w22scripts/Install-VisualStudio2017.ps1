# powershell script to install visual studio 2017 community on w22
#
# choco, see also https://learn.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2017/install/use-command-line-parameters-to-install-visual-studio?view=vs-2017
# and https://github.com/microsoft/vs-dockerfiles/blame/main/native-desktop/Dockerfile
Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
#
#choco install -y visualstudio2017buildtools --quiet --norestart 
choco install -y visualstudio2017community --quiet --norestart

#choco install visualstudio2017buildtools --norestart --quiet --packageParameters "--allWorkloads --includeRecommended --includeOptional --passive"



refreshenv

# save modified machine path
[System.Environment]::SetEnvironmentVariable('Path',$env:Path, 'Machine')




