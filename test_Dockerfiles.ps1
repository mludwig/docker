# powershell script
# create interactive test Dockerfiles from gitlab for windows variants (non interactive)
# just append an entry point at the end
$windowsVariants=( "w19", "w22" )

foreach ($wvar in $windowsVariants ) {
	echo "windows variant "$wvar
	
	$files = Get-ChildItem "Dockerfile.*$wvar"
	for ($i=0; $i -lt $files.Count; $i++) {
		# echo $files[$i].FullName
		$tfile = $files[$i].FullName + ".test" 
		#echo "writing "$tfile
		del $tfile
		copy $files[$i].FullName $tfile

		Add-Content $tfile "COPY ./entrypoint.ps1 /"
		Add-Content $tfile "ENTRYPOINT powershell.exe .\entrypoint.ps1"
		echo "test Dockerfile generated for "$tfile  

	}
}



 
