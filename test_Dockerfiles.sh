#!/bin/bash
# create interactive test Dockerfiles from gitlab for linux variants (non interactive)
# just append an entry point at the end
linuVariants=(
    "cal9"
    "cc7"
)
for lvar in "${linuVariants[@]}"; do
	echo "linux variants $lvar"
	for dd in `ls -1 ./Dockerfile.*.$lvar | grep -v "\.test"  ` 
	do
		#echo ${dd} 
		rm -f ${dd}.test
		cp ${dd} ${dd}.test
		echo "ADD entry.sh /" >> ${dd}.test
		echo "RUN chmod +x /entry.sh"  >> ${dd}.test
		echo "ENTRYPOINT [\"/entry.sh\"]" >> ${dd}.test
		echo "test Dockerfile generated for "${dd}.test   
	done
done





 
